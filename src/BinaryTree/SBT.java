/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BinaryTree;

import java.util.Comparator;

/**
 *
 * @author scarlet Espinoza
 */
public class SBT<E> {
    private NodeBT<E> root;
    private Comparator<E> f;
    
    public SBT(Comparator<E> f){
        root=null;
        this.f=f;
    }
    
    public boolean isEmpty(){
        return root==null;
    }
     public boolean add(E data){
        if(data==null)
            return false;
        this.root=add(data,root);
        return true;
    }
    
    private NodeBT<E> add(E data, NodeBT<E> p){
        if(p==null)
            p=new NodeBT<>(data);
        else if(f.compare(data, p.getData())<0)
            p.setLeft(add(data,p.getLeft()));
        else if(f.compare(data, p.getData())>0)
            p.setRight(add(data,p.getRight()));
        return p;
    }
    
    public E min(){
        if(root==null)
            return null;
        else 
            return min(root);
    }
    
    private E min(NodeBT<E> p){
        if(p.getLeft()==null)
            return p.getData();
        return min(p.getLeft());
    }
    
    
    public E max(){
        if(root==null)
            return null;
        else
            return max(root);
        
    }
    
    private E max(NodeBT<E> p){
        if(p.getRight()==null)
            return p.getData();
        else
            return max(p.getRight());
    }
    
    public void preOrden(){
        preOrden(root);
    }
    
    private void preOrden(NodeBT<E> node){
        if(node!=null){
            System.out.print(node.getData()+" ");
            preOrden(node.getLeft());
            preOrden(node.getRight());
        }
    }
    
    public void enOrden(){
        enOrden(root);
    }
    private void enOrden(NodeBT<E> node){
        if(node!=null){
            enOrden(node.getLeft());
            System.out.print(node.getData()+" ");
            enOrden(node.getRight());
        }
    }
    
    public void posOrden(){
        posOrden(root);
    }
    private void posOrden(NodeBT<E> node){
        if(node!=null){
            posOrden(node.getLeft());
            posOrden(node.getRight());
            System.out.print(node.getData()+" ");
        }
    }
    
    
    public boolean remove(E data){
        if(data==null || isEmpty())
            return false;
        root=remove(data,root);
        return true;
    }
    
    private NodeBT<E> remove(E element, NodeBT<E> n){
        if(n==null)
            return n;
        else if(f.compare(element, n.getData())<0)
            n.setLeft(remove(element,n.getLeft()));
        else if(f.compare(element, n.getData())>0)
            n.setRight(remove(element,n.getRight()));
        else if(n.getRight()!=null && n.getLeft()!=null){
            n.setData(max(n.getLeft()));//mayor de los menores cuando hay dos subarboles
            n.setRight(remove(n.getData(),n.getRight()));//eliminar antiguo nodo
        }else
            n=(n.getLeft()!=null)?n.getLeft():n.getRight();//puede tener subarbol derecho como no tener el izquierp o al reves
        return n;
        
    }    
}
