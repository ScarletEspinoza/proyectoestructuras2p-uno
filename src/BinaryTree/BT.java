/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BinaryTree;

/**
 *
 * @author scarlet Espinoza
 */
public class BT<E> {
    private NodeBT<E> root;
    
    public BT(){
        root=null;
    }
    
    public boolean isEmpty(){
        return root==null;
    }
    
   public boolean add(E child,E parent){
       NodeBT<E> NodeChild =new NodeBT<>(child);
        if(child==null){
            return false;
        }else if(parent ==null || isEmpty()){
            root=NodeChild;
             return true;
        } else if(parent!=null){
            if(SearchNode(child)==null){
                NodeBT<E> Np= SearchNode(parent);
                if(Np==null|| (Np.getLeft()!=null && Np.getRight()!=null )){ return false;
                } else if (Np.getLeft()==null){
                    Np.setLeft(NodeChild);
                } else {
                    Np.setRight(NodeChild);
                }
                return true;
            }
        }
        return false;
    
    }
   
   
   public NodeBT<E> SearchNode(E data){
        if(data==null || isEmpty() ) return null;
        return SearchNode(data,root);
    }
      
    private NodeBT SearchNode(E Data,NodeBT <E> n){
        if(n==null){ 
            return null;
        }else if(n.getData().equals(Data)){  
            return n;
        }else{
            NodeBT<E> L= SearchNode(Data,n.getLeft());
            return(L!=null)? L:SearchNode(Data,n.getRight());
           
        }
    
    }
    
    
    public int Altura(){
        return Altura(root);
    }
   
    private int Altura(NodeBT <E> n){
         if(n==null){
             return 0;
         }else{
             return 1 + Math.max(Altura(n.getLeft()), Altura(n.getRight()));
          }
        
      }
    
   
    public int contarHojas(){
       return contarHojas(root);
       
   }  
   private int contarHojas (NodeBT <E> n){
       if(n==null)return 0;
       else if(n.getRight()==null && n.getLeft()==null) return 1;
       return contarHojas(n.getLeft())+ contarHojas(n.getRight());
   }
      
   
    public boolean esLleno(){
     return esLleno(root);   
    }    
    private boolean esLleno(NodeBT <E> n){
        if(n==null)return true;
        else if((n.getLeft()==null && n.getRight()!=null)|| (n.getLeft()!=null && n.getRight()==null)) return false;
         return (esLleno(n.getLeft()) && esLleno(n.getRight())) && (Altura(n.getLeft())== Altura(n.getRight()));
    }
    
    
    public void preOrden(){
        preOrden(root);
    }
    private void preOrden(NodeBT <E> node){
        if(node!=null){
            System.out.print(node.getData()+" ");
            preOrden(node.getLeft());
            preOrden(node.getRight());
        }
    }
    
    public void enOrden(){
        enOrden(root);
    }
    private void enOrden(NodeBT <E> node){
        if(node!=null){
            enOrden(node.getLeft());
            System.out.print(node.getData()+" ");
            enOrden(node.getRight());
        }
    }
    
    public void posOrden(){
        posOrden(root);
    }
    private void posOrden(NodeBT <E> node){
        if(node!=null){
            posOrden(node.getLeft());
            posOrden(node.getRight());
            System.out.print(node.getData()+" ");
        }
    } 
    
    public NodeBT<E> getRoot(){
        return root;
    }
    
    public void setRoot(NodeBT<E> root){
        this.root = root;
    }

}