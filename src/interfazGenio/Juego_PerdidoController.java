/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfazGenio;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.Initializable;
import static interfazGenio.LecturaArchivo.escribirArchivo;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Juan Jose
 */

public class Juego_PerdidoController implements Initializable{

    @FXML
    private AnchorPane root;

    @FXML
    private Label lbl_animal;

    @FXML
    private Label lbl_pregunta;

    @FXML
    private Label lbl_rspta;

    @FXML
    private TextField input_animal;

    @FXML
    private TextField input_pregunta;

    @FXML
    private TextField input_rspta;

    @FXML
    private Button btn_terminar;

    @FXML
    private Button btn_repetir;
    
    private Button btn_guardar;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }    

    @FXML
    private void terminar(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void guardarNuevoAnimal(ActionEvent event) {
        String animal = input_animal.getText();
        String pregunta = input_pregunta.getText();
        String respuesta = input_rspta.getText();
        guardarArchivo(animal, pregunta, JuegoController.getUltimoAnimal(), respuesta);
        try {
            escribirArchivo();
            Alert alerta= new Alert(Alert.AlertType.INFORMATION);
            alerta.setTitle("Éxito");
            alerta.setContentText("Gracias por tu ayuda!");
            alerta.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(Juego_PerdidoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    @FXML
    void repetirJuego(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/interfazGenio/Juego.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Juego_PerdidoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        Stage stage = (Stage) btn_repetir.getScene().getWindow();
        stage.setScene(scene);
    }
   
    
    private void guardarArchivo(String animal, String pregunta, String pivot, String respuesta){
       
        List<String> array = LecturaArchivo.lineasArchivo;
        boolean encontrado = false;
        int contador = 0;
        while(contador < array.size() && !encontrado){
            if(array.get(contador).contains(pivot)){
                String nuevaRespuesta = "#R es un " + animal;
                String nuevaPregunta = "#P " + pregunta +"?";
                if(respuesta.equalsIgnoreCase("si")){
                    array.add(contador, nuevaRespuesta);    
                }else if(respuesta.equalsIgnoreCase("no")){
                    array.add(contador+1, nuevaRespuesta);
                }
                array.add(contador+2, nuevaPregunta);
                encontrado = true;
            }
            contador++;
        }
    }

}
