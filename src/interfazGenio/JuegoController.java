/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfazGenio;

import BinaryTree.BT;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import static interfazGenio.LecturaArchivo.crearArbol;
import BinaryTree.NodeBT;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class JuegoController implements Initializable {
    private final static String path = "src/Recursos/DatosGenioPolitecnico.txt";
    private BT<String> arbol = new BT<>();
    private static NodeBT<String> lineaActual;

    @FXML
    private Label preguntaLBL;
    @FXML
    private Button SBTN;
    @FXML
    private Button NBTN;
    @FXML
    private HBox buttonHBOX;
    @FXML
    private Button okBTN;
     @FXML
    private Button botonsalir;
       @FXML
    private Button botonjugardenuevo;
    @FXML
    private ImageView imageview;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {         
            arbol = crearArbol(path);
            lineaActual = arbol.getRoot();
            preguntaLBL.setText(lineaActual.getData());
        } catch (IOException ex) {
            Logger.getLogger(JuegoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        cargarImagen();
        
    }    
    
    
    private void cargarImagen(){
           Image image= new Image("/Recursos/Polito.jpg");
           imageview.setImage(image);
    }
    
    
    @FXML
    private void aceptar(ActionEvent event) {
        System.out.println("El jugador ha presionado: Si");
        lineaActual = lineaActual.getLeft();
        if(lineaActual == null){
            finalizarwin();
            preguntaLBL.setText("¡He acertado!");
        }
        else{
            preguntaLBL.setText(lineaActual.getData());
        }
        
    }
      
    @FXML
    private void negar(ActionEvent event) throws IOException {
        System.out.println("El jugador ha presionado: No");
        if(lineaActual.getRight()!=null){
            lineaActual = lineaActual.getRight();
            preguntaLBL.setText(lineaActual.getData());
        }
        else{
            preguntaLBL.setText("¡He perdido!");
           // preguntaLBL.setText("He perdido");
            finalizar();
            //perder();
            
        }
    }
 /*private void cambiarpantalla() throws IOException{
     Parent root = FXMLLoader.load(getClass().getResource("/interfazGenio/Juego_Perdido.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) boton.getScene().getWindow();
        stage.setScene(scene);
 }
     */
     
    private void finalizarwin(){
         SBTN.setVisible(false);
         NBTN.setVisible(false);
         botonsalir.setVisible(true);
         botonjugardenuevo.setVisible(true);
    }
    
    
    private void finalizar(){
            SBTN.setVisible(false);
            NBTN.setVisible(false);
            okBTN.setVisible(true);
           
    }
    @FXML
    private void endGame(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/interfazGenio/Juego_Perdido.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) okBTN.getScene().getWindow();
        stage.setScene(scene);
        stage.setResizable(false);
    }
 
       @FXML
    private void salir(ActionEvent event) throws IOException {
      System.exit(0);
    }
    
          @FXML
    private void jugarotravez(ActionEvent event) throws IOException {
        lineaActual = arbol.getRoot();
        preguntaLBL.setText(lineaActual.getData());
            SBTN.setVisible(true);
            NBTN.setVisible(true);
        botonsalir.setVisible(false);
        botonjugardenuevo.setVisible(false);
    }
    
     
    public static String getUltimoAnimal(){
        return lineaActual.getData();
    }
    
}
