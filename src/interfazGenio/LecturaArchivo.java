/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfazGenio;

import BinaryTree.BT;
import BinaryTree.NodeBT;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;


/**
 *
 * @author scarlet Espinoza
 */
public class LecturaArchivo {
    protected static final List<String> lineasArchivo = new ArrayList<>();
    private static final String PATH = "src/Recursos/DatosGenioPolitecnico.txt";

  private LecturaArchivo() {
    }
    
    
    
    
    
    public static BT<String> crearArbol(String path) throws  IOException{
        BT arbol = new BT<>();
        Deque<NodeBT<String>> pila = new LinkedList<>();
        try (BufferedReader sc = new BufferedReader(new FileReader(path))) {
            String linea;
            lineasArchivo.clear(); // limpia las lineas que estan demas en el archivo
            while((linea = sc.readLine()) != null){
                lineasArchivo.add(linea); // escribe las lineas que van a ser ingresadas en el archivo
                if(linea.contains("#P")){
                    NodeBT<String> hoja1 = pila.pop();
                    NodeBT<String> hoja2 = pila.pop();
                    NodeBT<String> pregunta = new NodeBT<>(linea.replace("#P","").trim());
                    pregunta.setRight(hoja1);
                    pregunta.setLeft(hoja2);
                    pila.push(pregunta);
                }
                else{
                    pila.push(new NodeBT<>(linea.replace("#R","").trim()));
                }
            }
        }
        arbol.setRoot(pila.pop());
        return arbol;
    }
    
    public static void escribirArchivo() throws IOException{
        File archivoViejo = new File(PATH);
     archivoViejo.delete();
        try (PrintWriter fw = new PrintWriter(PATH)) {
            for(String linea : lineasArchivo){
                fw.println(linea);
            }
        }
    }

    }
