/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfazGenio;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class MenuPrincipalController implements Initializable {

    @FXML
    private Button jugarBTN;
    private ImageView imageview;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void iniciar(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/interfazGenio/Juego.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) jugarBTN.getScene().getWindow();
        stage.setScene(scene);
        
    }
    
}
